import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup , FormControl} from '@angular/forms';
import { Router } from '@angular/router'
import { User} from '../models/user';
import { first } from 'rxjs/operators';

import { AuthService } from  '../auth.service';
@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {
  registerForm: FormGroup;
  isSubmitted = false;
// Variables Declared
email;
password;

userName;
confirmPassword;
constructor(private authService: AuthService, private router: Router,   private fb: FormBuilder){ 
  this.registerForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    userName: ['',[Validators.required]],
    password: ['',[Validators.required]],
    confirmPassword: ['',[Validators.required]]
});

}
get uf() { return this.registerForm.controls; }

  ngOnInit(): void {
  }
submit(){
  console.log(this.registerForm.value);
  this.isSubmitted = true;
  if(this.registerForm.invalid){
    return;
  }
  this.authService.login(this.registerForm.value);
  this.router.navigateByUrl('/login');
}

}
