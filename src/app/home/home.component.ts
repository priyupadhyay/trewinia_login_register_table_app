import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

import { OrganisationService} from '../organisation.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
orgList$ 
org :any []=[];
totalRec : number;
page: number =2;
searchedKeyword: string;
public data:any []=[];
public id :string;
public employee_Name: string;
public employee_salry :  string;
public employee_age : string;
public profile_image :string;
public status : string;

  constructor(private authService: AuthService, private router: Router, private orgService:OrganisationService) { }



  ngOnInit(): void {
    console.log(this.totalRec);
    console.log(this.page);
  }
  displayOrgList(){
 this.orgService.displayOrgList()
.subscribe(
  (data: any) =>{
    console.log(data.json);
    this.orgList$ = data
  })
}

  
  logout(){
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }
}
