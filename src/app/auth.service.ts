import { Injectable } from '@angular/core';
import { User } from './models/user';
import { HttpClient} from '@angular/common/http';
import { Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private token: string;
  constructor(private http: HttpClient, private router: Router) { }
  public login(userInfo: User){
    localStorage.setItem('ACCESS_TOKEN', "access_token");
  }
  public isLoggedIn(){
    return localStorage.getItem('ACCESS_TOKEN') !== null;
  
  
  }
  public logout(){
    this.token = "";
    localStorage.removeItem('ACCESS_TOKEN');
  }
}


