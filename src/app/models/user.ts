export interface User {
    id: number;
    email: string;
    password: string;
    token: string;
    userName: string;
}
interface TokenResponse {
    token: string;
  }
  
  export interface TokenPayload {
    email: string;
    password: string;
    userName?: string;
  }