import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router} from '@angular/router';
import { User } from './models/user';
// import { Organisation} from './home/organisation';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class OrganisationService {

  constructor(private http: HttpClient, private router: Router) { }
  displayOrgList() : Observable<object> {
 return this.http.get('/assets/data/orgList.json')
  }
}
