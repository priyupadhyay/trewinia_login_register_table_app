import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup , FormControl} from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router'
import { User} from '../models/user';
import { AuthService } from  '../auth.service';
// import {  AuthenticationService } from '../authentication.service';
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;
  isSubmitted = false;
// Variables Declared
email;
password;
returnUrl: string;


constructor(  private authService: AuthService, private fb: FormBuilder,private router: Router,  private route: ActivatedRoute,
    ){ 
    
  this.loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['',[Validators.required]],
});


}

get uf() { return this.loginForm.controls; }
  ngOnInit() {
 
  }
  
  login(form){
    console.log(this.loginForm.value);
    this.isSubmitted = true;
    if(this.loginForm.invalid){
      return;
    }
    this.authService.login(this.loginForm.value);
    this.router.navigateByUrl('/home');
  

  }
  forgotPassword(){
    console.log('Forgot Password');
   
  }

}
